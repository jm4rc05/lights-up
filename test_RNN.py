import unittest, numpy as np
from lightup import PuzzleBoard, PrintPuzzle, InitRNN, SolutionZip
from neupy import algorithms
import matplotlib.pyplot as plt


# Se 'detail == True', então informar 'rnn'
def printDebugInfo(data, puzzle, individual, expected, predicted, detail = False, rnn = None):


    def intValue(individual):

        return int("".join(str(i) for i in individual),2)


    if detail and rnn:
        print('=' * 80)
        for i, itemExpanded in enumerate(data):
            item = SolutionZip.contract(itemExpanded)
            print("Data({:04d}): ({:04d}) {}".format(i, intValue(item), item))
            PrintPuzzle.printSolution(puzzle, item)
            print('-' * 80)
        print('=' * 80)

    print('=' * 80)
    print("Individual: ({}) {}".format(intValue(individual), individual))
    print('-' * 80)
    PrintPuzzle.printSolution(puzzle, individual)
    print('=' * 80)

    print('=' * 80)
    print("Expected: ({}) {}".format(intValue(expected), expected))
    print('-' * 80)
    PrintPuzzle.printSolution(puzzle, expected)
    print('=' * 80)

    print('=' * 80)
    print("Predicted:  ({}) {}".format(intValue(predicted), predicted))
    print('-' * 80)
    PrintPuzzle.printSolution(puzzle, predicted)
    print('=' * 80)


class TestRNN(unittest.TestCase):


    def test_predict01_async(self):


        individual = [0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0]
        expected = [0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        rnn = algorithms.DiscreteHopfieldNetwork(mode='async', verbose = True, check_limit = True)
        data = InitRNN.initData(puzzle)
        rnn.train(np.array(data))
        prediction = rnn.predict(np.array(SolutionZip.expand(individual))).tolist()[0]
        _ = SolutionZip.contract(prediction)

        printDebugInfo(data, puzzle, individual, expected, _)

        self.assertEqual(_, expected, "Teste da rede neural falhou")


    def test_predict01_sync(self):


        individual = [0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0]
        expected = [0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        rnn = algorithms.DiscreteHopfieldNetwork(mode='sync', verbose = True, check_limit = True)
        data = InitRNN.initData(puzzle)
        rnn.train(np.array(data))
        prediction = rnn.predict(np.array(SolutionZip.expand(individual))).tolist()[0]
        _ = SolutionZip.contract(prediction)

        printDebugInfo(data, puzzle, individual, expected, _)

        self.assertEqual(_, expected, "Teste da rede neural falhou")


    def test_predict02_async(self):


        individual = [0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0]
        expected = [0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        rnn = algorithms.DiscreteHopfieldNetwork(mode='async', verbose = True, check_limit = True)
        data = InitRNN.initData(puzzle)
        rnn.train(np.array(data))
        prediction = rnn.predict(np.array(SolutionZip.expand(individual))).tolist()[0]
        _ = SolutionZip.contract(prediction)

        printDebugInfo(data, puzzle, individual, expected, _)

        self.assertEqual(_, expected, "Teste da rede neural falhou")


    def test_predict02_sync(self):


        individual = [0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0]
        expected = [0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        rnn = algorithms.DiscreteHopfieldNetwork(mode='sync', verbose = True, check_limit = True)
        data = InitRNN.initData(puzzle)
        rnn.train(np.array(data))
        prediction = rnn.predict(np.array(SolutionZip.expand(individual))).tolist()[0]
        _ = SolutionZip.contract(prediction)

        printDebugInfo(data, puzzle, individual, expected, _)

        self.assertEqual(_, expected, "Teste da rede neural falhou")


    # Exemplo do artigo, página 1405, segunda coluna
    def test_predict03_async(self):


        individual = [1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0]
        expected = [0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        rnn = algorithms.DiscreteHopfieldNetwork(mode='async', verbose = True, check_limit = True)
        data = InitRNN.initData(puzzle)
        rnn.train(np.array(data))
        prediction = rnn.predict(np.array(SolutionZip.expand(individual))).tolist()[0]
        _ = SolutionZip.contract(prediction)

        printDebugInfo(data, puzzle, individual, expected, _)

        self.assertEqual(_, expected, "Teste da rede neural falhou")


    # Exemplo do artigo, página 1405, segunda coluna
    def test_predict03_sync(self):


        individual = [1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0]
        expected = [0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        rnn = algorithms.DiscreteHopfieldNetwork(mode='sync', verbose = True, check_limit = True)
        data = InitRNN.initData(puzzle)
        rnn.train(np.array(data))
        prediction = rnn.predict(np.array(SolutionZip.expand(individual))).tolist()[0]
        _ = SolutionZip.contract(prediction)

        printDebugInfo(data, puzzle, individual, expected, _)

        self.assertEqual(_, expected, "Teste da rede neural falhou")


if __name__ == '__main__':
    unittest.main(argv=['ignored'], exit=False)