import time
from lightup import Solver


def main():
    startTime = time.time()

    config = Solver.PuzzleConfig(printOut = True)

    Solver.run(config, "exercicios/01.json")

    print("--- Tempo de execução: %s segundos ---" % (time.time() - startTime))

if __name__ == "__main__":
    main()