import unittest, numpy as np
from lightup import PuzzleBoard, PrintPuzzle, InitRNN, SolutionZip
from neupy import algorithms
import matplotlib.pyplot as plt


def intValue(individual):

    return int("".join(str(i) for i in individual),2)


def printDebugInfo(individual, expected, data = None):


    expectedValue = intValue(expected)
    print('=' * 80)
    print("Informed:  ({:04d}) - {}".format(intValue(individual), individual))
    print("Expected:  ({:04d}) - {}".format(expectedValue, expected))
    print('-' * 80)
    if data:
        for itemExpanded in data:
            item = SolutionZip.contract(itemExpanded)
            itemValue = intValue(item)
            print("Generated: ({:04d}) - {}".format(itemValue, item))
            if itemValue == expectedValue:
                print("Found!")
                break
        print('.' * 80)
    print('=' * 80)


class TestRNNInit(unittest.TestCase):


    def test_init01(self):


        individual = [0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0]
        expected = [0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        rnn = algorithms.DiscreteHopfieldNetwork(mode='sync', check_limit = True)
        data = InitRNN.initData(puzzle)
        found = False
        expectedValue = intValue(expected)
        for item in data:
            itemValue = intValue(SolutionZip.contract(item))
            if itemValue == expectedValue:
                found = True
                break

        printDebugInfo(individual, expected)

        self.assertTrue(found, "Teste da inicialização da rede neural falhou")


    def test_init02(self):


        individual = [0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0]
        expected = [0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        rnn = algorithms.DiscreteHopfieldNetwork(mode='sync', check_limit = True)
        data = InitRNN.initData(puzzle)
        found = False
        expectedValue = intValue(expected)
        for item in data:
            itemValue = intValue(SolutionZip.contract(item))
            if itemValue == expectedValue:
                found = True
                break

        printDebugInfo(individual, expected)

        self.assertTrue(found, "Teste da inicialização da rede neural falhou")


    # Exemplo do artigo, página 1405, segunda coluna
    def test_init03(self):


        individual = [1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0]
        expected = [0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        rnn = algorithms.DiscreteHopfieldNetwork(mode='async', check_limit = True)
        data = InitRNN.initData(puzzle)
        found = False
        expectedValue = intValue(expected)
        for item in data:
            itemValue = intValue(SolutionZip.contract(item))
            if itemValue == expectedValue:
                found = True
                break

        printDebugInfo(individual, expected)

        self.assertTrue(found, "Teste da inicialização da rede neural falhou")


if __name__ == '__main__':
    unittest.main(argv=['ignored'], exit=False)