$pdf_mode = 1; 
$pdflatex = 'pdflatex  %O -f --shell-escape %S';
$hash_calc_ignore_pattern{'pdf'}='^';
$clean_ext = 'acn bcf bbl dvi frm glg glo gls ist loa lol mw run.xml xwm';
add_cus_dep('glo', 'gls', 0, 'makeglo2gls');
sub makeglo2gls {
    system("makeindex -s '$_[0]'.ist -t '$_[0]'.glg -o '$_[0]'.gls '$_[0]'.glo");
}