import numpy as np, itertools
from lightup import PrintPuzzle, SolutionZip


# Obter o tamanho do quebra-cabeça no arquivo
def boardSize(puzzle):
    
    size = 0
    for line in puzzle:
        for cell in line:
            if(cell.idx != None):
                size += 1
    
    return size


#
# OBJETIVO:
#   obter uma lista de soluções possíveis e intermediárias para o problema
#   sem se preocupar com uma solução completa, mas apenas em reforçar a regra
#   de não ter acesa mais de uma lâmpada por linha e coluna simultaneamente
# 
def initData(puzzle, expansion = 196):


    def combination(puzzle):


        # Converte a lista binária da solução num inteiro
        # Em Python, até 63 bits (um reservado para sinal)
        def intValue(individual):

            return int("".join(str(i) for i in individual),2)


        # Retorna '1' se a posição 'j' pode ser ligada
        # ou '0' se não
        def lightValue(puzzle, individual, index):


            # Determina se uma célula está iluminada
            # Obs.: deve-se informar uma célula apagada, senão
            #       vai retornar True pois a própria está acesa!
            def inLight(puzzle, individual, x, y):
            
                # Verifica se tem alguma na coluna (para trás)
                for k in range(x, -1, -1):                      # variando 'x' -1 até 0 em 'k'
                    cell = puzzle[k][y]                         # com coluna em 'y' fixa
                    if cell.idx == None:
                        break                                   # Achou obstáculo (terminou)
                    else:
                        if individual[cell.idx] == 1:
                            return True                         # Achou uma lâmpada acesa

                # Verifica se tem alguma na coluna (para frente)
                for k in range(x, len(puzzle)):                 # variando 'x' até final em 'k'
                    cell = puzzle[k][y]                         # com coluna em 'y' fixa
                    if cell.idx == None:
                        break                                   # Achou obstáculo (terminou)
                    else:
                        if individual[cell.idx] == 1:
                            return True                         # Achou uma lâmpada acesa

                # Verifica se tem alguma na linha (para trás)
                for l in range(y, -1, -1):                      # variando 'y' -1 até 0 em 'l'
                    cell = puzzle[x][l]                         # com linha em 'x' fixa
                    if cell.idx == None:
                        break                                   # Achou obstáculo (terminou)
                    else:
                        if individual[cell.idx] == 1:
                            return True                         # Achou uma lâmpada

                # Verifica se tem alguma na linha (para frente)
                for l in range(y, len(puzzle[x])):              # variando 'y' até final em 'l'
                    cell = puzzle[x][l]                         # com linha em 'x' fixa
                    if cell.idx == None:
                        break                                   # Achou obstáculo (terminou)
                    else:
                        if individual[cell.idx] == 1:
                            return True                         # Achou uma lâmpada
                
                # Não achou nada
                return False


            result = 0
            
            # Procura 'j' entre as linhas...
            for k, line in enumerate(puzzle):
                # ... e colunas do tabuleiro
                for l, cell in enumerate(line):
                    if cell.idx == index:                       # Quando achar...
                        # Se estiver não iluminada por outra lâmapda...
                        if not inLight(puzzle, individual, k, l):
                            result = 1                      # ... pode ligar
                        return result

            return result


        # Verifica se a solução gerada é válida para
        # a regra de uma lâmpada ligada por segmento
        def valid(puzzle, individual):

            result = True

            if intValue(individual) == 0:
                # Se estiver tudo apagado então já é inválida
                result = False
            else:
                for i, cell in enumerate(individual):
                    if cell == 1: # Se for uma lâmpada ligada
                        # Cópia segura para manter 'individual' imutável
                        safeIndividual = individual.copy()
                        safeIndividual[i] = 0 # Desliga a célula a testar
                        if lightValue(puzzle, safeIndividual, i) == 0:
                            # A lâmpada deveria estar apagada então
                            # é uma solução inválida
                            result = False
                            break


            return result


        products = [[]]

        # Um produto cartesiana de uma lista de 0's e 1's
        # com o tamanho de uma solução
        for value in [(0, 1)] * boardSize(puzzle):
            products = [x + [y] for x in products for y in value]

        # Converte para uma lista com as soluções válidas
        # geradas pelo produto
        result = [list(SolutionZip.expand(product, expansion)) for product in products if valid(puzzle, list(product))]

        return result


    # Todas variações de células acesas e apagadas possível
    data = combination(puzzle)

    return data