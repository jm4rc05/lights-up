from itertools import count


def expand(solution, multiplier = 196):

    result = [point for cell in solution for point in [cell] * multiplier]

    return result


def contract(solution, divider = 196):

    result = [solution[i] for i in range(0, len(solution), divider)]

    return result