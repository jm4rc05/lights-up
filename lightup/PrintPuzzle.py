# Imprime uma solução (uma lista simples) com base
# na estrutura do tabuleiro informado
def printSolution(puzzle, individual = None):
    
    print('-' * 80)

    for line in puzzle:
        for cell in line:
            if cell.value == 0:
                if(individual == None):
                    print("-", end = " ")
                elif cell.idx != None:
                    if individual[cell.idx] == 0:
                        print(" ", end = " ")
                    else:
                        print("*", end = " ")
                else:
                    print(cell.value, end = " ")
            elif cell.value == None:
                print("#", end = " ")
            else:
                print(cell.value, end = " ")
        print()

    print('=' * 80)

    return