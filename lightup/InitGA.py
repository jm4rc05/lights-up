# Obter o tamanho do quebra-cabeça no arquivo
def boardSize(puzzle):
    
    size = 0
    for line in puzzle:
        for cell in line:
            if(cell.idx != None):
                size += 1
    
    return size


def fitness(puzzle, individual): 


    # Definir a função objetivo com base na regra do
    # número de células acesas ao redor de uma célula
    # preta
    def fitnessGeral(individual, puzzle): 


        # Retorna número de lâmpadas ligadas nos quatro 
        # lados de uma célula
        def sumNeighbors(individual, i, j, puzzle):

            def getNeighborValue(k, l):
                if puzzle[k][l].idx != None:
                    return individual[puzzle[k][l].idx]
                else:
                    return 0

            neighborsValues = 0

            if i > 0: # norte
                neighborsValues += getNeighborValue(i - 1, j)
            
            if j > 0: # oeste
                neighborsValues += getNeighborValue(i, j - 1)

            if i < len(puzzle) - 1: # sul
                neighborsValues += getNeighborValue(i + 1, j)

            if j < len(puzzle[0]) - 1: # leste
                neighborsValues += getNeighborValue(i, j + 1)

            return neighborsValues


        result = 0
        # Somar as regras para as células pretas
        for i, line in enumerate(puzzle):
            for j, cell in enumerate(line):
                if cell.idx == None and cell.value != None:      # Célula preta e com valor
                    result += sumNeighbors(individual, i, j, puzzle) - cell.value

        return result


    # Retorna o número de celulas não iluminadas pela solução
    def shadows(individual, puzzle):

        # Feixe de luz sobre uma célula
        def lightUpCell(ind, i, j):

            breakable = False

            if puzzle[i][j].idx == None:
                breakable = True
            else:
                ind[puzzle[i][j].idx] = 1
            
            return breakable

        # Feixe de luz sobre uma coluna
        def lightUpColumn(ind, i, j):

            for k in range(j, 0, -1):
                if lightUpCell(ind, k, j):
                    break

            for k in range(j, len(puzzle)):
                if lightUpCell(ind, k, j):
                    break

            return

        # Feixe de luz sobre uma linha
        def lightUpLine(ind, i, j):

            for k in range(j, 0, -1):
                if lightUpCell(ind, i, k):
                    break

            for k in range(j, len(puzzle[0])):
                if lightUpCell(ind, i, k):
                    break

            return

        individualCopy = individual.copy()                      # cria uma cópia que irá conter as
                                                                # células acesas

        # Percorre todo o tabuleiro buscando lampadas ligadas
        for i, line in enumerate(puzzle):                       # Para cada linha...
            for j, cell in enumerate(line):                     # ... e cada célula da linha...
                if cell.idx != None:                            # .... se for uma célula branca...
                    if individualCopy[cell.idx] == 1:           # ... e estiver ligada...
                        lightUpLine(individualCopy, i, j)       # ... ilumina a linha...
                        lightUpColumn(individualCopy, i, j)     # ... e a coluna

        # Ao final, individualCopy irá conter todas as células que foram acesas no processo
        # e as que não foram estão na sombra!
        shadowsCount = individualCopy.count(0)

        return shadowsCount


    # Curiosidade!!!
    # Se acrescentar na função objetivo uma contabilização de linhas e colunas com
    # lâmpadas duplicadas, isto bastaria para que o algorítmo genético por si próprio
    # encontrasse uma solução
    return (fitnessGeral(individual, puzzle) + shadows(individual, puzzle),)