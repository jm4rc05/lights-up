import random, string, json, numpy as np, matplotlib.pyplot as plt
from array import array
from deap import base, creator, tools
from neupy import algorithms, plots
from lightup import PuzzleBoard, PrintPuzzle, InitGA, InitRNN, SolutionZip


class PuzzleConfig():

    def __init__(self, populationSize = None, CXPB = None, MUTPB = None, INDPB = None, tournSize = None, maxIter = None, RNN = None, printOut = None):
        # Numero de indivíduos a cada geração
        self.populationSize = populationSize or 100
        # CXPB  : probabilidade de dois indivíduos cruzarem
        # MUTPB : probabilidade de haver mutação do indivíduo
        # INDPB : probabilidade de mutação pela troca de um atributo por outro 
        #         vindo do pool de genes
        self.CXPB = CXPB or 0.6
        self.MUTPB = MUTPB or 0.01
        self.INDPB = INDPB or 0
        # Número de indivíduos participando da seleção
        # Não entendi bem este parâmetro
        # - aparentemente, pelo que diz a documentação, escolhe aleatoriamente esse 
        #   número de indivíduos
        #   e seleciona os melhores
        # - se este número for muito grande, o cálculo demora bem mais sem melhoria 
        #   na população de modo
        #   geral, e o resultado acaba sendo o mesmo, que é atingir a função objetivo
        self.tournSize = tournSize or 3
        # Máximo de iterações para atingor o objetivo e evitar que demore demais
        self.maxIter = maxIter or 100
        # Executa ou não a rede neural (que não presta para nada)
        self.RNN = RNN or True
        # Imprime mensagens de progresso
        self.printOut = printOut or False
    

# LOW   : menor valor possível
# UP    : maior valor possível
LOW, UP = 0.0, 1.0

creator.create("FitnessMin", base.Fitness, weights = (-1.0,))
creator.create("Individual", list, fitness = creator.FitnessMin)

def run(config: PuzzleConfig, puzzle, expansion = 196):
    puzzle = PuzzleBoard.initPuzzle(puzzle)

    # Algoritmo Genético
    # pylint: disable=no-member
    toolbox = base.Toolbox()
    toolbox.register("attr_lamp", random.randint, LOW, UP)
    toolbox.register("individual", tools.initRepeat, creator.Individual,
                    toolbox.attr_lamp, InitRNN.boardSize(puzzle))
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    toolbox.register("evaluate", InitGA.fitness, puzzle)
    #toolbox.register("mate", tools.cxUniform, indpb=INDPB)
    toolbox.register("mate", tools.cxOnePoint)
    toolbox.register("mutate", tools.mutUniformInt, low = LOW, up = UP, 
                    indpb = config.INDPB)
    toolbox.register("select", tools.selTournament, tournsize = 3)

    pop = toolbox.population(config.populationSize)
    fitnesses = list(map(toolbox.evaluate, pop))
    for ind, fit in zip(pop, fitnesses):
        ind.fitness.values = fit
    fits = [ind.fitness.values[0] for ind in pop]

    rnn = algorithms.DiscreteHopfieldNetwork(mode='sync', check_limit = True)

    # Rede Neural
    if config.RNN:
        data = InitRNN.initData(puzzle)
        rnn.train(np.array(data))


    # Abordagem Híbrida
    g = 0
    max_fits_ant = 0
    min_fits_ant = 0
    media_ant = 0
    desvio_ant = 0
    if config.printOut:
        print("Algorítmo genético rodando com {} gerações de {} indivíduos - melhores: ({}), piores: ({})".
            format(config.maxIter, config.populationSize, min(fits), max(fits)))
    while g < config.maxIter:
        g = g + 1

        offspring = toolbox.select(pop, config.populationSize)
        offspring = list(map(toolbox.clone, offspring))

        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            if random.random() < config.CXPB:
                toolbox.mate(child1, child2)
                if config.RNN:
                    prediction = rnn.predict(np.array(SolutionZip.expand(child1, expansion))).tolist()[0]
                    child1[:] = SolutionZip.contract(prediction, expansion)
                del child1.fitness.values
                if config.RNN:
                    prediction = rnn.predict(np.array(SolutionZip.expand(child2, expansion))).tolist()[0]
                    child2[:] = SolutionZip.contract(prediction, expansion)
                del child2.fitness.values

        for mutant in offspring:
            if random.random() < config.MUTPB:
                toolbox.mutate(mutant)
                if config.RNN:
                    prediction = rnn.predict(np.array(SolutionZip.expand(mutant, expansion))).tolist()[0]
                    mutant[:] = SolutionZip.contract(prediction, expansion)
                del mutant.fitness.values
            
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitnesses = map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        pop[:] = offspring
        
        fits = [ind.fitness.values[0] for ind in pop]

        media = sum(fits) / config.populationSize
        desvio = abs(sum(x*x for x in fits) / config.populationSize - media**2)**0.5

        max_fits = max(fits)
        min_fits = min(fits)
        if g == config.maxIter or max_fits != max_fits_ant or min_fits != min_fits_ant or media != media_ant or desvio != desvio_ant:
            max_fits_ant = max_fits
            min_fits_ant = min_fits
            media_ant = media
            desvio_ant = desvio
            if config.printOut:
                print("Geração: {} - melhor: ({}), pior: ({}), média: ({}), desvio: ({})".
                    format(g, min(fits), max(fits), media, desvio))

        # Já achei o melhor!!!
        if min(fits) == 0:
            break

    fits = [ind.fitness.values[0] for ind in pop]
    solutions = []
    for i, ind in enumerate(pop):
        if sum(ind.fitness.values) == min(fits):
            solutions.append(pop[i])
            if config.printOut:
                print(''.join([char*80 for char in '*']))
                print("Indivíduo[{:04d}]: {} com ({}) pontos".
                    format(i, ind, ind.fitness.values[0]))
                print(''.join([char*80 for char in '.']))
                PrintPuzzle.printSolution(puzzle, pop[i])
                print(''.join([char*80 for char in '*']))
            break
    if len(solutions) == 0:
        if config.printOut:
            print("Não encontrada solução!!!")

    toolbox.unregister("attr_lamp")
    toolbox.unregister("individual")
    toolbox.unregister("population")
    toolbox.unregister("evaluate")
    toolbox.unregister("mate")
    toolbox.unregister("mutate")


    return solutions


def graph():
    plt.figure(figsize=(12, 12))
    plt.title("Diagrama Hinton")
    plots.hinton(rnn.weight)
    plt.show()