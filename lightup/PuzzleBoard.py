import json

# Classe que irá manter o tabuleiro
class Lamp(object):
    
    def __init__(self):
        self.idx = None
        self.value = None
        
    def __repr__(self):
        i = self.__idx
        if i == None:
            i = "@"
        v = self.__value
        if v == None:
            v = "#"

        return "{\"" + str(v) + "\"[" + str(i) + "]}"
    
    # Contém um indexador que irá apontar para uma posição
    # numa estrutura de dados (uma lista simples) contendo
    # a solução para o tabuleiro
    # Para os quatrados pretos conterá "None"
    @property
    def idx(self):
        return self.__idx
    
    # Para as células pretas contém um número (vazio ou zero 
    # a quatro) informando a restrição de quantas células 
    # vizinhas devem ser preenchidas com lâmpadas
    # O vazio será representado por "#"
    # Para os quadrados brancos conterá "None"
    @property
    def value(self):
        return self.__value
    
    @idx.setter
    def idx(self, idx):
        self.__idx = idx

    @value.setter
    def value(self, value):
        self.__value = value


#
# Definir o quebra-cabeça a partir do arquivo no formato JSON
# fornecido, por exemplo:
#
# {
#     "puzzle": [
#         ["-", "-", "-", "-"],
#         ["-", "3", "-", "2"],
#         ["-", "-", "-", "-"],
#         ["-", "1", "-", "#"]
#     ]
# }
#
# A célula pode conter:
# "-"     : indica uma célula em branco
# "#"     : indica uma célula preta
# "0"-"4" : indica uma célula preta com numeral dentro
#
def initPuzzle(filename):
    # ... lendo a partir de um arquivo
    with open(filename, "r") as puzzle:
        # ... contendo um arquivo JSON
        contents = json.load(puzzle)

    puzzleSize = 0
    puzzleLines = 0
    lamps = []
    # O arquivo possui listas de linhas
    for line in contents['puzzle']:
        # ... cada linha com uma célula
        puzzleColumns = 0
        lamps.append([])
        for cell in line:
            lamps[puzzleLines].append(Lamp())
            if cell == '-':
                # Registramos uma lâmpada
                lamps[puzzleLines][puzzleColumns].idx = puzzleSize
                lamps[puzzleLines][puzzleColumns].value = 0
                # Contamos as células em branco
                puzzleSize += 1                
            elif cell == "#":
                # Registramos uma célula preta sem restrição
                lamps[puzzleLines][puzzleColumns].value = None
            else:
                # Registramos uma célula preta com restrição de 
                # lâmpadas acesas ao redor
                lamps[puzzleLines][puzzleColumns].value = int(cell)
                
            # Contabilizamos as colunas
            puzzleColumns += 1
            
        # Contabilizamos as linhas
        puzzleLines += 1
    
    return lamps