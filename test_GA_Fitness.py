import unittest
from lightup import InitGA, PuzzleBoard, PrintPuzzle


class TestGAFitness(unittest.TestCase):


    def test_fitness01(self):

        individual = [1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/07.json")
        print("\nIndividual: {}".format(individual))
        PrintPuzzle.printSolution(puzzle, individual)

        _ = InitGA.fitness(puzzle, individual)
        self.assertNotEqual(_, (0,), "Teste da função objetivo falhou")


    def test_fitness02(self):

        individual = [0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        print("\nIndividual: {}".format(individual))
        PrintPuzzle.printSolution(puzzle, individual)

        _ = InitGA.fitness(puzzle, individual)
        self.assertEqual(_, (0,), "Teste da função objetivo falhou")


    def test_fitness03(self):

        individual = [0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/07.json")
        print("\nIndividual: {}".format(individual))
        PrintPuzzle.printSolution(puzzle, individual)

        _ = InitGA.fitness(puzzle, individual)
        self.assertEqual(_, (0,), "Teste da função objetivo falhou")


    def test_fitness04(self):

        individual = [0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        print("\nIndividual: {}".format(individual))
        PrintPuzzle.printSolution(puzzle, individual)

        _ = InitGA.fitness(puzzle, individual)
        self.assertEqual(_, (0,), "Teste da função objetivo falhou")


    def test_fitness05(self):

        individual = [0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        print("\nIndividual: {}".format(individual))
        PrintPuzzle.printSolution(puzzle, individual)

        _ = InitGA.fitness(puzzle, individual)
        self.assertEqual(_, (0,), "Teste da função objetivo falhou")

    def test_fitness06(self):

        individual = [0, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        print("\nIndividual: {}".format(individual))
        PrintPuzzle.printSolution(puzzle, individual)

        _ = InitGA.fitness(puzzle, individual)
        self.assertEqual(_, (0,), "Teste da função objetivo falhou")


    def test_fitness07(self):

        individual = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        print("\nIndividual: {}".format(individual))
        PrintPuzzle.printSolution(puzzle, individual)

        _ = InitGA.fitness(puzzle, individual)
        self.assertNotEqual(_, (0,), "Teste da função objetivo falhou")


    def test_fitness08(self):

        individual = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        print("\nIndividual: {}".format(individual))
        PrintPuzzle.printSolution(puzzle, individual)

        _ = InitGA.fitness(puzzle, individual)
        self.assertNotEqual(_, (0,), "Teste da função objetivo falhou")

    
    def test_fitness09(self):

        a = [0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0]
        b = [0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        print("\nIndividual A: {}".format(a))
        PrintPuzzle.printSolution(puzzle, a)
        fitA = InitGA.fitness(puzzle, a)
        print("fitness: {}".format(fitA))
        print("\nIndividual B: {}".format(b))
        PrintPuzzle.printSolution(puzzle, b)
        fitB = InitGA.fitness(puzzle, b)
        print("fitness: {}".format(fitB))

        self.assertLess(fitA, fitB, "Teste da função objetivo falhou")


    def test_fitness10(self):

        a = [0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0]
        b = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        print("\nIndividual A: {}".format(a))
        PrintPuzzle.printSolution(puzzle, a)
        fitA = InitGA.fitness(puzzle, a)
        print("fitness: {}".format(fitA))
        print("\nIndividual B: {}".format(b))
        PrintPuzzle.printSolution(puzzle, b)
        fitB = InitGA.fitness(puzzle, b)
        print("fitness: {}".format(fitB))

        self.assertLess(fitA, fitB, "Teste da função objetivo falhou")


    def test_fitness11(self):

        a = [0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0]
        b = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        print("\nIndividual A: {}".format(a))
        PrintPuzzle.printSolution(puzzle, a)
        fitA = InitGA.fitness(puzzle, a)
        print("fitness: {}".format(fitA))
        print("\nIndividual B: {}".format(b))
        PrintPuzzle.printSolution(puzzle, b)
        fitB = InitGA.fitness(puzzle, b)
        print("fitness: {}".format(fitB))

        self.assertLess(fitA, fitB, "Teste da função objetivo falhou")


    def test_fitness12(self):

        a = [0, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0]
        b = [0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0]
        puzzle = PuzzleBoard.initPuzzle("exercicios/01.json")
        print("\nIndividual A: {}".format(a))
        PrintPuzzle.printSolution(puzzle, a)
        fitA = InitGA.fitness(puzzle, a)
        print("fitness: {}".format(fitA))
        print("\nIndividual B: {}".format(b))
        PrintPuzzle.printSolution(puzzle, b)
        fitB = InitGA.fitness(puzzle, b)
        print("fitness: {}".format(fitB))

        self.assertLess(fitA, fitB, "Teste da função objetivo falhou")


if __name__ == '__main__':
    unittest.main(argv=['ignored'], exit=False)