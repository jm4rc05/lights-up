import unittest
from lightup import SolutionZip


class TestSolutionZip(unittest.TestCase):

    
    def test_expansion01(self):

        individual = [0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0]
        expected = [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]

        _ = SolutionZip.expand(individual, 2)

        self.assertEqual(_, expected, "Teste de expansão da solução falhou")

    
    def test_expansion02(self):

        individual = [0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0]
        expected = [0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0]

        _ = SolutionZip.expand(individual, 1)

        self.assertEqual(_, expected, "Teste de expansão da solução falhou")

    
    def test_expansion03(self):

        individual = [0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0]
        expected = []

        _ = SolutionZip.expand(individual, 0)

        self.assertEqual(_, expected, "Teste de expansão da solução falhou")


    def test_contraction01(self):

        individual = [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]
        expected = [0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0]

        _ = SolutionZip.contract(individual, 2)

        self.assertEqual(_, expected, "Teste de expansão da solução falhou")


    def test_contraction02(self):

        individual = [0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0]
        expected = [0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0]

        _ = SolutionZip.contract(individual, 1)

        self.assertEqual(_, expected, "Teste de expansão da solução falhou")


if __name__ == '__main__':
    unittest.main(argv=['ignored'], exit=False)