import unittest
from lightup import Solver


class TestLightUp(unittest.TestCase):


    def test_solve01(self):
        _ = Solver.run(Solver.PuzzleConfig(printOut = True), "exercicios/01.json")
        self.assertGreater(len(_), 0, "Solução encontrada")


    def test_solve02(self):
        _ = Solver.run(Solver.PuzzleConfig(printOut = True), "exercicios/02.json")
        self.assertGreater(len(_), 0, "Solução encontrada")


    def test_solve07(self):
        _ = Solver.run(Solver.PuzzleConfig(printOut = True), "exercicios/07.json")
        self.assertGreater(len(_), 0, "Solução encontrada")


if __name__ == '__main__':
    unittest.main(argv=['ignored'], exit=False)